#!/bin/bash


replaces=libboost-all-dev
conflicts=libboost-all-dev
pkgversion=1.0
pkgname=drainware-boost
pkggroup=drainware
pkgarch=amd64
maintainer=jose.palanco@drainware.com
requires=''
command="./b2 install"



if [ $# -eq 1  ]
then
sudo checkinstall -y --replaces=$replaces --conflicts=$conflicts --nodoc --pkgversion=$pkgversion \
--pkgrelease=$1 --type=debian --pkgname=$pkgname --pkggroup=$pkggroup --pkgarch=$pkgarch \
--maintainer=$maintainer --requires=$requires --install=no  $command
mv *.deb ..


else
echo "usage: $0 [RELEASE]"
fi


